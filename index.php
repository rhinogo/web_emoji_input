<!DOCTYPE HTML>
<html>
<head>
    <title>title</title>
    <meta charset="utf-8">
<style type="text/css">
.maxwidth{
	max-width:600px;
	margin:0 auto;
	background:#FFF;
	height:100%;
    -webkit-box-shadow: 0 2px 2px rgba(125,125,125,0.2);
    -moz-box-shadow: 0 2px 2px rgba(125,125,125,0.2);
    box-shadow: 0 2px 2px rgba(125,125,125,0.2);
}
article {
	line-height: 20px;
	height: 100px;
	-webkit-box-sizing: border-box;
	background: #ffffff;
	word-break: break-all;
	word-wrap: break-word;
	white-space: pre-line;
	overflow: hidden;
	overflow-y: auto;
	border: 1px solid #eee;
}
img.emoji {
	height: 1.5em;
	width: 1.5em;
	margin: 0 .05em 0 .1em;
	vertical-align: -0.2em;
}
</style>
</head>
<body>
<div class="maxwidth">
	<article id="form_article" contenteditable="true" onpaste="myInput.listen(this, event);" onkeydown="myInput.listen(this, event);" oninput="myInput.listen(this, event);" onpropertychange="myInput.listen(this, event);" ></article>
	<button type="button" onclick="submit()" style="margin-top: 20px;">保存</button>
</div>
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/twemoji/1.4.1/twemoji.min.js"></script>
<script src="./js/input.js"></script>
<script>
	function submit(){
		var article = document.getElementById("form_article").innerHTML;
		if(article.length == 0){
			alert("请输入内容", 2000);return;
		}
		
		try 
		{
			$.ajax({
				url: 'mpost.php',
				type: 'POST',
				data:{article:article},
				dataType: 'json',
				timeout: 5000,
				error: function() {
					alert('网络错误');
				},
				success: function(result){
					if(result){
						alert(result.input);
					}
				}
			});
		} catch (e) {
			alert("对不起！崩溃了,请重试", 2000);
		} 
	}
</script>
</body>
</html>